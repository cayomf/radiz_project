SELECT funcionario.id_funcionario
FROM db_radix.dbo.funcionario
JOIN db_radix.dbo.funcionario_projeto ON funcionario_projeto.id_funcionario = funcionario.id_funcionario
JOIN db_radix.dbo.projeto on projeto.id_projeto = funcionario_projeto.id_projeto
WHERE funcionario.uf = 'RJ' GROUP BY funcionario.id_funcionario HAVING (2020 - COUNT(funcionario.ano_nascimento)) > 20 AND COUNT(funcionario_projeto.id_projeto) > 2