using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite o numero de ordem da sequencia de Fibonacci desejado:");
            ulong Fibonacci(ulong fn) => fn == 0 || fn == 1 ? 1 : Fibonacci(fn - 2) + Fibonacci(fn - 1);
            ulong order = Convert.ToUInt64(Console.ReadLine());
            Console.WriteLine(Fibonacci(order));
            Console.ReadLine(); // Para nao fechar o console

        }
    }
}