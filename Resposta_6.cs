using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NumberSix
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] possib = { "pedra", "papel", "tesoura", "lagarto", "Spock" };
            List<List<String>> combinations = new List<List<string>>();
            List<String> sheldonChoices = new List<string>();
            List<String> rajChoices = new List<string>();
            Boolean goToResults = false;
            List<int> results = new List<int>();
            int times = 0;
            bool check = false;

            String[] SplitString (String str)
            {
                String[] tempAnswers = str.Split(Convert.ToChar(" "));
                //sheldonChoices.Add(tempAnswers[0]);
                //rajChoices.Add(tempAnswers[1]);
                return tempAnswers;
            }


            Console.WriteLine( "Quantas rodadas serao?" );
            while (check == false)
            {

                String input = Console.ReadLine().ToString();

                if (Regex.IsMatch( input, "[0-9]") && Convert.ToInt32(input) < 101 )
                {
                    times = Convert.ToInt32(input);
                    check = true;
                    break;
                }
                Console.WriteLine( "Valor invalido! Tente Novamente:");
            }

            void SheldonVictory(String[] tempAnswers)
            {
                if ( tempAnswers[0] == "pedra" && ( tempAnswers[1] == "tesoura" || tempAnswers[1] == "lagarto") )
                {
                    results.Add(1);
                }
                if ( tempAnswers[0] == "papel" && ( tempAnswers[1] == "pedra" || tempAnswers[1] == "Spock") )
                {
                    results.Add(1);
                }
                if ( tempAnswers[0] == "tesoura" && ( tempAnswers[1] == "papel" || tempAnswers[1] == "lagarto") )
                {
                    results.Add(1);
                }
                if ( tempAnswers[0] == "lagarto" && ( tempAnswers[1] == "Spock" || tempAnswers[1] == "papel") )
                {
                    results.Add(1);
                }
                if ( tempAnswers[0] == "Spock" && ( tempAnswers[1] == "pedra" || tempAnswers[1] == "tesoura") )
                {
                    results.Add(1);
                }
            }

            void RajVictory ( String[] tempAnswers )
            {
                if ( tempAnswers[1] == "pedra" && ( tempAnswers[0] == "tesoura" || tempAnswers[0] == "lagarto" ) )
                {
                    results.Add(2);
                }
                if ( tempAnswers[1] == "papel" && ( tempAnswers[0] == "pedra" || tempAnswers[0] == "Spock" ) )
                {
                    results.Add(2);
                }
                if ( tempAnswers[1] == "tesoura" && ( tempAnswers[0] == "papel" || tempAnswers[0] == "lagarto" ) )
                {
                    results.Add(2);
                }
                if ( tempAnswers[1] == "lagarto" && ( tempAnswers[0] == "Spock" || tempAnswers[0] == "papel" ) )
                {
                    results.Add(2);
                }
                if ( tempAnswers[1] == "Spock" && ( tempAnswers[0] == "pedra" || tempAnswers[0] == "tesoura" ) )
                {
                    results.Add(2);
                }
            }

            Console.WriteLine("Quais foram as escolhas de Sheldon e Raj, respectivamente?");


            for (int i = 0; i < times; i++)
            {
                Boolean goNext = false;



                while (goNext == false)
                {
                    String input = Console.ReadLine().ToString();

                    if (input.Count(f => f == ' ') == 1)
                    {
                        String[] tempAnswers = SplitString(input);

                        if (possib.Contains(tempAnswers[0]) && possib.Contains(tempAnswers[1]))
                        {
                            //SplitString(input);
                            //goNext = true;
                            // break;
                            //int caseSwitch = 1;


                            if (tempAnswers[0] != tempAnswers[1])
                            {
                                SheldonVictory(tempAnswers);
                                RajVictory(tempAnswers);
                                

                            }
                            else
                            {
                                results.Add(3);
                            }
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("String invalida! Tente Novamente:");
                        }
                    }
                    else
                    {
                        Console.WriteLine("String invalida! Tente Novamente:");
                    }




                }
            }


                for ( int i = 0; i < results.Count; i++ )
            {
                if ( results[i] == 1 )
                {
                    Console.WriteLine("Caso #1: Bazinga!");
                }
                if (results[i] == 2)
                {
                    Console.WriteLine("Caso #2: Raj trapaceou!");
                } else
                {
                    Console.WriteLine("Caso #3: De novo!");
                }

            }

            Console.ReadLine();


        }
    }
}
