using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NumberFive
{
    class Program
    {
        static void Main(string[] args)
        {
            int times = 0;
            List<String> strings = new List<string>();
            List<String> pals = new List<string>();


            string ReverseString ( string rev ) => rev.Reverse()
                                               .Select( x => x.ToString() )
                                               .Aggregate( (a, b) => a + b );

            IEnumerable<string> CutString( string red, int size )
            {
                for ( int i = 0; i < red.Length - size + 1; i++ )
                {
                    yield return red.Substring( i, size );
                }
            }


            bool CheckPalindrome( string pal ) => pal.Length == 1 ? false : pal.Equals( ReverseString(pal) );


            void SearchPalindrome()
            {
                for ( int i = 0; i < strings.Count; i++ )
                {

                    if ( CheckPalindrome(strings[i]) )
                    {
                        pals.Add( strings[i] );
                    } else
                    {
                        for ( int j = strings[i].Length - 1; j >= 2; j-- )
                        {
                            List<string> takes = CutString( strings[i], j ).Where( x => CheckPalindrome(x) ).ToList();

                            if ( takes.Count() != 0 )
                            {
                                string result = takes.First();
                                pals.Add(result);
                                break;
                            } 
                        }
                    }
                    if ( pals.Count < i+1 )
                    {
                        pals.Add("");
                    }
                }
            }



            bool check = false;
            Console.WriteLine( "Quantas strings voce ira colocar?" );
            while ( check == false )
            {

                String input = Console.ReadLine().ToString();

                if ( Regex.IsMatch( input, "[0-9]" ) )
                {
                    times = Convert.ToInt32(input);
                    check = true;
                    break;
                }
                Console.WriteLine( "Valor invalido! Tente Novamente:" );
            }
            Console.WriteLine( "Entre com as strings(Cada string tem o maximo de 100 caracteres):" );
            for ( int i = 0; i < times; i++ )
            {
                Boolean goNext = false;

                while ( goNext == false )
                {
                    String input = Console.ReadLine().ToString();


                    if ( Regex.IsMatch( input, "[a-z]" ) && input.Length < 101 )
                    {
                        strings.Add( input );
                        goNext = true;
                        break;
                    }
                    Console.WriteLine( "String invalida! Tente Novamente:" );
                }

            }

            SearchPalindrome();

            for ( int i = 0; i < pals.Count; i++ )
            {
                if ( pals[i].Length == 0 )
                {
                    Console.WriteLine( "consulta inválida" );
                } else
                {
                    Console.WriteLine(pals[i]);
                }
                
                
            }

            Console.ReadLine();

        }
    }
}
