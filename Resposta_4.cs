using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NumberFour
{
    class Program
    {
        static void Main(string[] args)
        {
            int times = 0;
            String[] alpha = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            List<List<String>> letters = new List<List<string>>();
            List<List<int>> repets = new List<List<int>>();
            List<List<String>> singleLetters = new List<List<string>>();
            List<String> strings = new List<string>();
            List<String> options = new List<string>();
            List<int> numOptions = new List<int>();

            
            Console.WriteLine( "Quantas strings voce ira colocar?" );
            times = Convert.ToInt32( Console.ReadLine() );
            Console.WriteLine( "Entre com as strings(Cada string tem o maximo de 100 caracteres):");
            for ( int i =  0; i < times; i++ )
            {
                Boolean goNext = false;
                

                while ( goNext == false )
                {
                    String input = Console.ReadLine().ToString();

                    int numberSpaces = input.Count(f => f.ToString() == " ");
                    if ( numberSpaces == 1 )
                    {
                        String[] parts = input.Split(' ');
                        if ( Regex.IsMatch(parts[1], "[a-z]") && parts[0].Length < 101 && parts[1].Length == 1 )
                        {
                            strings.Add( parts[0] );
                            options.Add( parts[1] );
                            goNext = true;
                            break;
                        } 
                            Console.WriteLine( "String invalida! Tente Novamente:" );
                        
                    }
                    else
                    {
                        Console.WriteLine("String invalida! Tente Novamente:");
                    }
                }

            }

            for ( int i = 0; i < strings.Count; i++ )
            {
                List<String> localLetters = new List<string>();
                for ( int j = 0; j < strings[i].Length; j++ )
                {
                    for ( int k = 0; k < alpha.Length; k++ )
                    {
                        if ( strings[i][j].ToString() == alpha[k] )
                        {
                            localLetters.Add(strings[i][j].ToString());
                        }
                    }
                }
                letters.Add(localLetters);
            }

            for ( int i = 0; i < letters.Count; i++ )
            {
                
                for ( int j = 0; j < letters[i].Count; j++ )
                {
                    List<String> localSingleLetters = new List<string>();
                    if ( singleLetters.Count == j )
                    {
                        singleLetters.Add(localSingleLetters);
                    }
                    if (!singleLetters[i].Contains(letters[i][j]))
                    {
                        singleLetters[i].Add(letters[i][j]);
                    }
                }
            }

            while ( singleLetters.Count != letters.Count )
            {
                for (int i = 0; i < singleLetters.Count; i++)
                {
                    if (singleLetters[i].Count == 0)
                    {
                        singleLetters.Remove(singleLetters[i]);
                    }
                }
            }

            for ( int i = 0; i < letters.Count; i++ )
            {
                for ( int j = 0; j < singleLetters[i].Count; j++ )
                {
                    int letterTimes = 0;
                    List<int> localRepets = new List<int>();

                    for ( int k = 0; k < letters[i].Count; k++ )
                    {
                        if ( letters[i][k] == singleLetters[i][j] )
                        {
                            letterTimes++;
                        }
                    }

                    if ( repets.Count == i )
                    {
                        repets.Add(localRepets);
                    }
                    repets[i].Add(letterTimes);
                }
            }

            for ( int i = 0; i < options.Count; i++ )
            {
                int localNumOptions = 0;
                for ( int j = 0; j < letters.Count; j++ )
                {
                    for ( int k = 0; k < letters[j].Count; k++ )
                    {
                        if ( letters[j][k] == options[i] )
                        {
                            localNumOptions++;
                        }
                    }
                }
                numOptions.Add(localNumOptions);
            }

            for ( int i = 0; i < singleLetters.Count; i++ )
            {
                String result = "";
                for ( int j = 0; j < singleLetters[i].Count; j++ )
                {
                    if ( j < singleLetters[i].Count-1 )
                    {
                        result = result + singleLetters[i][j] + "(" + repets[i][j] + ")" + "; ";
                    } else
                    {
                        result = result + singleLetters[i][j] + "(" + repets[i][j] + ")";
                    }
                }
                Console.WriteLine(result);
                if ( numOptions[i] != 0 )
                {
                    Console.WriteLine(numOptions[i]);
                } else
                {
                    Console.WriteLine("consulta invalida");
                }
            }

            Console.ReadLine();

        }
    }
}
